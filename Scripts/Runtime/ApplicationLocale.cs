﻿namespace SimianX.Localization
{
    /// <summary>
    /// Stores all language codes supported by the Application
    /// </summary>
    public class ApplicationLocale
    {
        public const string EN = "EN";
        public const string FR = "FR";
    }
}