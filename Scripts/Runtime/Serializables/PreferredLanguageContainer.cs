﻿using System;
using UnityEngine;

namespace SimianX.Localization.Serializables
{
    /// <summary>
    /// Container for storing the user's language preference
    /// </summary>
    [Serializable]
    public class PreferredLanguageContainer
    {
        private const string LANGUAGE_KEY = "LanguageKey";

        public const string RootDirectory = "Preferences";
        public const string Path = RootDirectory + "/" + "LanguageCode.json";

        public string languageCode;

        public PreferredLanguageContainer()
        {
            languageCode = LocaleHelper.GetSupportedLanguageCode(); // Default to the system locale if supported
        }

        public PreferredLanguageContainer(string languageCode)
        {
            this.languageCode = languageCode;
        }

        /// <summary>
        /// Will attempt to load the user's preffered language from the container's file path
        /// </summary>
        /// <returns>
        /// Will either return the user's language preference or determine a suitable default with LocaleHelper
        /// </returns>
        public static string LoadLanguageCode()
        {
            string languagePref = PlayerPrefs.GetString(LANGUAGE_KEY);

            if (string.IsNullOrEmpty(languagePref))
            {
                return LocaleHelper.GetSupportedLanguageCode();
            }

            return languagePref;
        }

        /// <summary>
        /// Will save the container to its file path
        /// </summary>
        /// <param name="preferredLanguageCode">
        /// Container that stores the language code
        /// </param>
        public static void SaveLanguageCode(PreferredLanguageContainer preferredLanguageCode)
        {
            PlayerPrefs.SetString(LANGUAGE_KEY, preferredLanguageCode.languageCode);
        }
    }
}