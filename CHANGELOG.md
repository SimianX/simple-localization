## [0.2.3] - 2021-03-18
### Fixed
- Fixed Localized Text Componenets included that were included in the loading scene being unable to wait for Localization Manager Initialization

## [0.2.2] - 2021-03-18
### Fixed
- Fixed Localized Text Components from not localizing if the language override occured while the component was disabled in the scene

## [0.2.1] - 2021-03-18
### Fixed
- Fixed Android and WebGL versions from being unable to retrieve localization files from the StreamingAssets directory

## [0.2.0] - 2021-03-09
### Added
- Added option to send UnityWebRequests to fetch language files